% Accueil SLAM
% par [moulinux](https://moulinux.frama.io/)
% (révision 04.09.2023)

# Introduction
* [Compétences][competences] en développement d'applications
* [Aide mémoire][git] GIT
* [Présentation des épreuves][ccf] en CCF
* poursuite d'études [après un BTS SIO - SLAM][post-bts]

# Documentation technique
* [CedFX] (version de la calculette éco-déplacement en JavaFX)
* [pyFormation][pyFormation_0-0] (version 0.0)
* [pyFormation][pyFormation_1-0] (version 1.0)


[competences]: https://s2sio.frama.io/competences
[CedFX]: http://ced.frama.io/CedFx
[pyFormation_0-0]: https://slam.frama.io/formation/pyFormation_0-0
[pyFormation_1-0]: http://slam.frama.io/formation/pyFormation_1-0
[git]: git.html
[ccf]: ccf.html
[post-bts]: post-bts.html
