% Aide mémoire GIT
% par [moulinux](https://moulinux.frama.io/)
% (révision 30.04.2021)

Cette page ne vise pas l'exhaustif, mais plutôt la synthèse...

# Les commandes du quotidien
Cloner un dépôt existant (avec SSH) :
```bash
git clone git@framagit.org:mes_groupes/mon_projet.git
```
Créer la branche `dev` et basculer dessus :
```bash
git checkout -b dev
```
Réaliser un `commit` sur tous les fichiers suivis et modifiés en enregistrant un message de description :
```bash
$ git commit -a -m "Résumé de la modification apportée"
```
Synchroniser la branche `origin` avec la branche locale `dev`. Et indiquer `origin` comme le dépôt distant par défaut :
```bash
git push -u origin dev
```


# Autres commandes indispensables
Cloner la branche `dev` d'un dépôt existant (avec SSH) :
```bash
git clone --branch dev git@framagit.org:mes_groupes/mon_projet.git
```
Vérifier l'état de votre répertoire de travail :
```bash
git status
```
Ajouter un fichier à l'index :
```bash
git add <mon_fichier>
```
Changer de branche :
```bash
git checkout <votre-branche>
```
Afficher tous les commits :
```bash
git log
```


# Des commandes plus rares
* Revenir au HEAD du dépôt distant (exemple ici sur la branche `master`) :
```bash
git checkout -B master origin/master
```
* Synchroniser les nouvelles branches distantes :
```bash
git fetch origin
```
* Lister toutes les branches (locales et distantes) :
```bash
git branch -a
```
* Créer une une branche de travail locale (à partir d'une distante) :
```bash
git checkout -b dev origin/dev
```
* Changer de dépôt `origin` :
```bash
git remote set-url origin git@framagit.org:mes_groupes/mon_projet.git
```
* Pousser toutes les branches :
```bash
git push -u origin --all
```
