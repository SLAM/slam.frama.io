% Idées pour une poursuite d'études après un BTS SIO - SLAM
% par [moulinux](https://moulinux.frama.io/)
% (révision 21.11.2023)

Cette page ne prétend pas viser l'exhaustivité

# Horizon des formations
* [BUT][but-info-clermont] (bac+3): Il est possible d'intégrer une formation BUT.
* Licences Pro (bac+3): Elles ont souvent été absorbées
par les BUT, mais il reste encore quelques formations...
* [Miage] (bac +5)
* École d'[ingénieurs][ingenieurs] (bac+5):
c'est un diplôme contrôlé par l'État, qui exige notamment qu'une partie
des enseignants travaillent également dans des laboratoires de recherche.
L'informatique est souvent une spécialité parmi d'autres formations.
* Écoles d'informatique en 3 ou 5 ans

# Formations "locales"
* Le [but informatique d'Aubière][but-info-clermont] (Clermont-Fd) propose deux spécialisations :
Développement d'applications pour plateformes mobiles; Développement d'applications web
* [BUT Métiers du Multimédia et de l'Internet (Le Puy)][BUT-multimedia-Le-Puy]
* [ISIMA] : Grande école publique d'Ingénieurs en Informatique
* [INSA-Lyon]

# Licences professionnelles
* Développeur Web Full Stack ([lien site univ][univ-lp-fullstack] et [lien école][ecole-lp-fullstack])
* Web-designer intégrateur ([lien site univ][univ-lp-beb-designer] et [lien école][ecole-lp-beb-designer])

# Autres formations
* [Le réseau MIAGE][reseau-miage]
* Page ["Se former au logiciel libre"][se-former-au-logiciel-libre] de l'April

[Licence_pro]: https://fr.wikipedia.org/wiki/Licence_pro
[Miage]: https://fr.wikipedia.org/wiki/Master_de_m%C3%A9thodes_informatiques_appliqu%C3%A9es_%C3%A0_la_gestion_des_entreprises
[ingenieurs]: https://fr.wikipedia.org/wiki/%C3%89tudes_d%27ing%C3%A9nieurs_en_France
[but-info-clermont]: https://iut.uca.fr/formations/but-informatique-clermont
[BUT-multimedia-Le-Puy]: https://iut.uca.fr/formations/but-metiers-du-multimedia-et-de-linternet-le-puy
[ISIMA]: https://www.isima.fr/
[INSA-Lyon]: https://www.insa-lyon.fr/fr/formation/informatique
[reseau-miage]: http://www.miage.fr/reseau-miage-france/
[se-former-au-logiciel-libre]: https://www.april.org/se-former-au-logiciel-libre
[univ-lp-fullstack]: https://formations.univ-larochelle.fr/lp-developpeur-web-full-stack
[ecole-lp-fullstack]: https://ecoleduweb.univ-larochelle.fr/parcours-metier/developpeur-web-full-stack/
[univ-lp-beb-designer]: https://formations.univ-larochelle.fr/lp-web-designer-integrateur
[ecole-lp-beb-designer]: https://ecoleduweb.univ-larochelle.fr/parcours-metier/web-designer-integrateur/
