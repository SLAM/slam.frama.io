% Épreuves du BTS SIO en CCF
% par [moulinux](https://moulinux.frama.io/)
% (révision 04.09.2023)


## Introduction

* Présentation basée sur des extraits du [référentiel] SIO
* Les extraits sont orientés pour les étudiants en formation initiale (sous statut scolaire)
* Pas de revendication d'une paternité des droits


## Présentation de l'épreuve E4
### Support et mise à disposition de services informatiques

* Épreuve pratique et orale – Coefficient 4
* **Objectif**: évaluer l'acquisition des [compétences] du bloc B1

## Modalités d'évaluation de l'épreuve E4

* Épreuve orale ;
* Basé sur des réalisations professionnelles vécues en AP et durant les stages ;
* Les situations professionnelles présentés doivent être conformes à un cadre établi ;
* Un *dossier numérique* doit être constitué et remis à une date fixée par l'établissement.

## Dossier numérique pour l'épreuve E4

* Un support retraçant le parcours de professionnalisation : c'est le **portfolio**...
* Un tableau de synthèse récapitulant l’ensemble des réalisations présentées (application tableur) ;
* Les deux attestations de stage.

## Déroulement de l’épreuve E4

* Deux phases consécutives pour une durée totale de **40 minutes** ;
* Pendant 10 minutes (maximum), présentation du parcours de professionnalisation (basé sur le portfolio) ;
* 30 minutes d'échange avec le jury :
    * **Objectif** : apprécier la capacité à mobiliser les compétences visées ;
    * rendre compte d'un travail réalisé au sein d'une équipe projet.

## Jury de l’épreuve E4

* Votre enseignant responsable de votre spécialité ;
* Un professionnel (généralement le maître de stage d'un de vos camarades),
* qui peut être remplacé par une personne en charge de l'enseignement d'un bloc professionnel.

## Engagement étudiant

* [Épreuve facultative][engagement] qui s'appuie sur l'épreuve E4
* Pour tous ceux qui exercent :
    * une activité bénévole au sein d'une association ;
    * une activité professionnelle ;
    * une activité militaire dans la réserve opérationnelle ;
    * un engagement de sapeur-pompier volontaire ;
    * un service civique.

## Exemples d'engagement étudiant

* Contributions soutenues au développement du logiciel libre (l'association Framasoft encadre certains développements) ;
* Former et équiper des populations en difficulté ou défavorisées pour permettre l'accès à l'informatique et à Internet à tous (exemple [libraisol]) ;
* Auto-entreprenariat.


## Présentation de l'épreuve E5
### Conception et développement d’applications

* Épreuve pratique et orale – Coefficient 4


[référentiel]: https://www.reseaucerta.org/sites/default/files/sio/BTS_ServicesInformatiquesOrganisations2019.pdf
[compétences]: https://s2sio.frama.io/competences
[engagement]: https://www.education.gouv.fr/bo/20/Hebdo38/ESRS2019793A.htm
[libraisol]: https://libraisol.fr
